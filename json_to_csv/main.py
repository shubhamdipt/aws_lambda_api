import csv
import os
import boto3
from time import gmtime, strftime

aws_access_key_id = os.environ.get('ACCESS_KEY_ID')
aws_secret_access_key = os.environ.get('SECRET_ACCESS_KEY')
aws_bucket = os.environ.get('S3_BUCKET')


class CSVDocument:

    def __init__(self):
        self.file_name = 'Transaction_{}.csv'.format(
                strftime("%Y_%m_%d_%H_%M_%S", gmtime()))
        self.file_path = '/tmp/{}'.format(self.file_name)

    def write_to_csv(self, list_data):
        with open(self.file_path, 'w+') as f:
            output = csv.writer(f, delimiter=',', )
            output.writerow(list_data)
            print('File printed.')

    def upload_file_to_s3(self):
        s3 = boto3.client(
            's3',
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key
        )
        with open(self.file_path, 'rb') as data:
            s3.upload_fileobj(data, aws_bucket, self.file_name)
            print('File uploaded.')


def lambda_handler(event, context):
    error = event.get('error')
    source = event.get('source')
    if not error and source:
        try:
            new_document = CSVDocument()
            data = [source.get('customer'), event.get('amount'),
                    event.get('currency')]
            new_document.write_to_csv(data)
            new_document.upload_file_to_s3()
            return {'status': 'SUCCESS'}
        except Exception as e:
            print('File could not be generated. Error: {}'.format(str(e)))
    return {'status': 'Error found or Source data not found.'}
