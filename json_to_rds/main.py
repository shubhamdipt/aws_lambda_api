import os
import psycopg2

postgres_host = os.environ.get('POSTGRES_HOST')
postgres_port = os.environ.get('POSTGRES_PORT')
postgres_user = os.environ.get('POSTGRES_USER')
postgres_password = os.environ.get('POSTGRES_PASSWORD')
postgres_database = os.environ.get('POSTGRES_DATABASE')

TABLE_NAME = os.environ.get('POSTGRES_TABLE_NAME')


def make_conn():

    conn = None
    try:
        conn = psycopg2.connect(host=postgres_host,
                                user=postgres_user,
                                password=postgres_password,
                                database=postgres_database,
                                port=postgres_port)
    except:
        print("Unable to connect to the database")
    return conn


def execute_query(conn, query):
    print("Now executing: %s" % (query))
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()
    conn.close()


def lambda_handler(event, context):
    error = event.get('error')
    source = event.get('source')
    if not error and source:
        amount = event['amount']
        currency = event['currency']
        customer = source['customer']
        conn = make_conn()
        if conn:
            query = "INSERT INTO {table_name} (customer, amount, currency) " \
                    "VALUES ({customer}, {amount}, {currency});".\
                format(table_name=TABLE_NAME, customer=customer,
                       amount=amount, currency=currency)
            execute_query(conn, query)
            return {'status': 'SUCCESS'}
        print('Database connection error')
        return {'status': 'Database connection error'}
    return {'status': 'Error found or Source data not found.'}
