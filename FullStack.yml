AWSTemplateFormatVersion: 2010-09-09
Description: 'AWS CloudFormation: API Gateway triggering Lambda function'
Resources:
  StripeLambda:
    Type: 'AWS::Lambda::Function'
    Properties:
      Code:
        S3Bucket: aws-lambda-zipped-files
        S3Key: executor.zip
      Description: An executor function
      FunctionName: StripeLambda
      Handler: main.lambda_handler
      Role: !GetAtt
        - LambdaExecutionRole
        - Arn
      Runtime: python3.6
  StripeLambdaJsonToRds:
    Type: 'AWS::Lambda::Function'
    Properties:
      Code:
        S3Bucket: aws-lambda-zipped-files
        S3Key: json_to_rds.zip
      Description: JSON to RDS function
      FunctionName: StripeLambdaJsonToRds
      Handler: main.lambda_handler
      Role: !GetAtt
        - LambdaExecutionRole
        - Arn
      Runtime: python3.6
  StripeLambdaJsonToCsv:
    Type: 'AWS::Lambda::Function'
    Properties:
      Code:
        S3Bucket: aws-lambda-zipped-files
        S3Key: json_to_csv.zip
      Description: JSON to CSV function
      FunctionName: StripeLambdaJsonToCsv
      Handler: main.lambda_handler
      Role: !GetAtt
        - LambdaExecutionRole
        - Arn
      Runtime: python3.6
  LambdaExecutionRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole'
  StripeApi:
    Type: 'AWS::ApiGateway::RestApi'
    Properties:
      Name: Stripe API
      Description: API used for Stripe webhooks.
      FailOnWarnings: true
  LambdaPermission:
    Type: 'AWS::Lambda::Permission'
    Properties:
      Action: 'lambda:invokeFunction'
      FunctionName: !GetAtt
        - StripeLambda
        - Arn
      Principal: apigateway.amazonaws.com
      SourceArn: !Join
        - ''
        - - 'arn:aws:execute-api:'
          - !Ref 'AWS::Region'
          - ':'
          - !Ref 'AWS::AccountId'
          - ':'
          - !Ref StripeApi
          - /*
  ApiGatewayCloudWatchLogsRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - apigateway.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Policies:
        - PolicyName: ApiGatewayLogsPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 'logs:CreateLogGroup'
                  - 'logs:CreateLogStream'
                  - 'logs:DescribeLogGroups'
                  - 'logs:DescribeLogStreams'
                  - 'logs:PutLogEvents'
                  - 'logs:GetLogEvents'
                  - 'logs:FilterLogEvents'
                Resource: '*'
  ApiGatewayAccount:
    Type: 'AWS::ApiGateway::Account'
    Properties:
      CloudWatchRoleArn: !GetAtt
        - ApiGatewayCloudWatchLogsRole
        - Arn
  StripeApiStage:
    DependsOn:
      - ApiGatewayAccount
    Type: 'AWS::ApiGateway::Stage'
    Properties:
      DeploymentId: !Ref ApiDeployment
      MethodSettings:
        - DataTraceEnabled: true
          HttpMethod: '*'
          LoggingLevel: INFO
          ResourcePath: /*
      RestApiId: !Ref StripeApi
      StageName: LATEST
  ApiDeployment:
    Type: 'AWS::ApiGateway::Deployment'
    DependsOn:
      - StripeRequest
    Properties:
      RestApiId: !Ref StripeApi
      StageName: DummyStage
  StripeResource:
    Type: 'AWS::ApiGateway::Resource'
    Properties:
      RestApiId: !Ref StripeApi
      ParentId: !GetAtt
        - StripeApi
        - RootResourceId
      PathPart: transaction
  StripeRequest:
    DependsOn: LambdaPermission
    Type: 'AWS::ApiGateway::Method'
    Properties:
      AuthorizationType: NONE
      HttpMethod: POST
      Integration:
        Type: AWS
        IntegrationHttpMethod: POST
        Uri: !Join
          - ''
          - - 'arn:aws:apigateway:'
            - !Ref 'AWS::Region'
            - ':lambda:path/2015-03-31/functions/'
            - !GetAtt
              - StripeLambda
              - Arn
            - /invocations
        IntegrationResponses:
          - StatusCode: 200
      ResourceId: !Ref StripeResource
      RestApiId: !Ref StripeApi
      MethodResponses:
        - StatusCode: 200
  StatesExecutionRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - !Sub states.eu-central-1.amazonaws.com
            Action: 'sts:AssumeRole'
      Path: /
      Policies:
        - PolicyName: StatesExecutionPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 'lambda:InvokeFunction'
                Resource: '*'
  TransactionStateMachine:
    Type: 'AWS::StepFunctions::StateMachine'
    Properties:
      DefinitionString:
        !Sub
          - |-
            {
              "Comment": "Parallel processing of Transaction data.",
              "StartAt": "Executor",
              "States": {
                "Executor": {
                  "Type": "Task",
                  "Resource": "${lambdaArn1}",
                  "Next": "Parallel"
                },
                "Parallel": {
                  "Type": "Parallel",
                  "Next": "Final State",
                  "Branches": [
                    {
                      "StartAt": "JsonToRDS",
                      "States": {
                        "JsonToRDS": {
                          "Type": "Task",
                          "Resource": "${lambdaArn2}",
                          "End": true
                        }
                      }
                    },
                    {
                      "StartAt": "JsonToCSV",
                      "States": {
                        "JsonToCSV": {
                          "Type": "Task",
                          "Resource": "${lambdaArn3}",
                          "End": true
                        }
                      }
                    }
                  ]
                },
                "Final State": {
                  "Type": "Pass",
                  "End": true
                }
              }
            }
          - {
          lambdaArn1: !GetAtt [ StripeLambda, Arn ],
          lambdaArn2: !GetAtt [ StripeLambdaJsonToRds, Arn ],
          lambdaArn3: !GetAtt [ StripeLambdaJsonToCsv, Arn ]
          }
      RoleArn: !GetAtt [ StatesExecutionRole, Arn ]
Outputs:
  RootUrl:
    Description: Root URL of the Stack
    Value: !Join
      - ''
      - - 'https://'
        - !Ref StripeApi
        - .execute-api.
        - !Ref 'AWS::Region'
        - .amazonaws.com
