# API Gateway --  Lambda -- Step Functions

## Objective:

- Building a Python 3.6.2 based ETL pipeline using Lambda, StepFunctions and API-Gateway using Cloudformation.
- An incoming webhook is triggering a lambda function executor which will execute a StepFunction. 
- The layout of the StepFunction can have different designs (in this case, subsequently two lambda functions are executed in parallel). 
- But two things should happen within the StepFunction:
	-The posted JSON body should be inserted to a Postgres DB hosted by RDS. 
	-The posted JSON body is stored in CSV format as an object in S3.





## Create Stack
./setup.sh <S3-Bucket-Key>
    
- This script creates three zipped files for 3 AWS Lambda Functions (executor, json_to_rds, json_to_csv). 
- These three zipped files are uploaded to the S3-Bucket.
- Finally, a stack is created using the CloudFormation template (FullStack.yml).
	
## Stripe Webhook API
- Path URI: /transaction/
- Request method: POST
- Sample JSON Response from Stripe: charge_succeeded.json

## First Lambda Function (executor):

- Triggered by POST response (/transaction/)
- Process the POST response and extract 'object' from the JSON data.
- Passes the data['object'] to two other lambda functions, i.e., json_to_rds and json_to_csv using AWS Step Functions

## Second Lambda Function (json_to_rds):

- The relevant data are inserted to a table of PostgreSQL database using psycopg2.
- From the data['object'], amount and currency are determined and from data['object']['source'], the customer id is extracted.
- ENVIRONMENT VARIABLES required:
	- POSTGRES_HOST
	- POSTGRES_PORT
	- POSTGRES_USER
	- POSTGRES_PASSWORD
	- POSTGRES_DATABASE
	- POSTGRES_TABLE_NAME
- Database Table with fields: customer, amount, currency
- From the POST data received from First Lambda Function, customer, amount and currency are inserted to the table.


## Third Lambda Function (json_to_csv)

- The same relevant data is also received here from the First Lambda Function in parallel to Second Lambda Function (check Cloudformation template: Step Functions).
- ENVIRONMENT VARIABLES required:
	- AWS_ACCESS_KEY_ID
	- AWS_SECRET_ACCESS_KEY
	- S3_BUCKET
- The relevant data from every such request is saved in a separate CSV file and pushed to the S3 Bucket.

THANK YOU !
