#!/usr/bin/env bash

BUILD_CONTENT=build

rm -rf ./build/
mkdir -p build

cd ./executor/
zip -r ../build/executor.zip *

cd ../json_to_csv/
zip -r ../build/json_to_csv.zip *

cd ../json_to_rds/
zip -r ../build/json_to_rds.zip *

cd ..

S3_BUCKET=$1

aws s3 cp "./build/executor.zip" "s3://$S3_BUCKET/executor.zip"
aws s3 cp "./build/json_to_csv.zip" "s3://$S3_BUCKET/json_to_csv.zip"
aws s3 cp "./build/json_to_rds.zip" "s3://$S3_BUCKET/json_to_rds.zip"

aws cloudformation create-stack --stack-name api-lambda-steps --template-body file://FullStack.yml --capabilities CAPABILITY_IAM

