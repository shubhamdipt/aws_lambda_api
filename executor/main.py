def lambda_handler(event, context):
    try:
        source_data = event['data']['object']
    except Exception as e:
        source_data = {'error': 'Transaction object not found.'}
    return source_data
